/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Chat;

import java.net.*;
import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Scanner;

/**
 *
 * @author ivan
 */
public class Cliente {
    
    BufferedReader in;
    PrintWriter    out;
    
    Socket cnx; //OBJETO SOCKET es un link que une dos PCs para compartir informacion mediante una direcc IP y un numero de puerto
                //obJETO DE LA CLASE SOCKET coN LA QUE SE CONECTA AL SERVER
    
    public static void main(String args[]) {
       
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                (new Cliente()).start();
            }
        });
    }

    void start() {
        String respuesta="", send = "";
        Conexion hilo;
        Scanner consola = new Scanner(System.in);
        try {                               //SE ABRE UN SOCKET con EL CUAL CONECTARSE, es la suma de la direccion y el puerto
            this.cnx = new Socket("192.168.1.106",4444); //nombre del socket CLIENTE que es 25.50.228.152 y el numero del puerto 
            
            in = new BufferedReader(new InputStreamReader(cnx.getInputStream()));//INPUT STREAM PARA RECIBIR INFORMACION DEL EXTERIROR
            out = new PrintWriter(cnx.getOutputStream(),true);//OUTPUT STREAM PARA ENVIAR AL EXTERIOR DEL PROGRAMA
            
            hilo = new Conexion(in);
            hilo.start(); //Hilo encargado de lecturas del servidor

            while (!send.toLowerCase().contains("salir")){   //este while se encarga de leer lo que el usuario teclea y lo mando al server
                 send = consola.nextLine();
                 out.println(send);                
            }
            
            hilo.ejecutar = false;
            Thread.sleep(2000);
            
            this.cnx.close();
            
        } catch (IOException ex) {
            Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
        }      
    }
    
    class Conexion extends Thread {
        public boolean ejecutar = true;
        BufferedReader in;
        
        public Conexion(BufferedReader in){//CONSTRUCTOR DONDE SE RECIBEN DATOS DEL EXTERIOR 
            this.in = in;
        }
        
        @Override
        public void run() {//SU UNICO TRABAJO ES ESTAR LEYENDO LA ENTRADA E IMPRIMIR LO QUELLEGUE
            String respuesta = "";
            while(ejecutar){
                try {            
                    respuesta = in.readLine();
                    if (respuesta!=null) System.out.println(respuesta); //AQUI SE IMPRIMEN LOS MENSAJES DEL EXTERIOR
                } catch (IOException ex) {
                    Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
                } 
            }
        }   
    }
}
