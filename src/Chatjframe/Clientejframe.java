
package Chatjframe;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class Clientejframe extends javax.swing.JFrame { 
    BufferedReader in;
    PrintWriter    out;
    Conexion       hilo;
    Socket cnx;
    
    boolean autent = false;
    public Clientejframe() {
        initComponents();
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//para cerrar la ventana
    }

     
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        chat_area = new javax.swing.JTextArea();
        write_message = new javax.swing.JTextField();
        btnsend = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        usuarioson = new javax.swing.JTextField();
        btn_salir = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        btn_actualizar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setAutoRequestFocus(false);
        setBackground(new java.awt.Color(0, 204, 0));

        chat_area.setColumns(20);
        chat_area.setRows(5);
        jScrollPane1.setViewportView(chat_area);

        write_message.setText(" ");

        btnsend.setFont(new java.awt.Font("Verdana", 1, 12)); // NOI18N
        btnsend.setText("ENVIAR");
        btnsend.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsendActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Impact", 0, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(51, 255, 51));
        jLabel1.setText("WHATSAPP 2");

        usuarioson.setEditable(false);
        usuarioson.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        usuarioson.setText(" ");
        usuarioson.setAlignmentY(0.1F);
        usuarioson.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                usuariosonActionPerformed(evt);
            }
        });

        btn_salir.setText("Salir");
        btn_salir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_salirActionPerformed(evt);
            }
        });

        jLabel4.setText("Contactos en Linea");

        btn_actualizar.setText("Actualizar");
        btn_actualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_actualizarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 311, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(write_message, javax.swing.GroupLayout.PREFERRED_SIZE, 218, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnsend)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(usuarioson, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addComponent(btn_salir))
                            .addComponent(btn_actualizar))
                        .addGap(38, 38, 38))))
            .addGroup(layout.createSequentialGroup()
                .addGap(93, 93, 93)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(258, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 31, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(usuarioson, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btn_actualizar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_salir))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 312, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(write_message, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnsend, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)))))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btn_salirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_salirActionPerformed
          try {//accion perfomada al clickear el boton
            this.out.println("salir"); //se envia mediante el objeto de exportacion el mensaje de salir
            this.hilo.ejecutar = false; 
            Thread.sleep(1500);         
            this.cnx.close();//se cierra la conexion del socket
        } catch (IOException ex) {//catch para evitar la caida del programa a lahora de cerrar la conexion
            Logger.getLogger(Clientejframe.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            Logger.getLogger(Clientejframe.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.dispose();
    }//GEN-LAST:event_btn_salirActionPerformed

    private void usuariosonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_usuariosonActionPerformed
         
    }//GEN-LAST:event_usuariosonActionPerformed

    private void btnsendActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsendActionPerformed
       
        this.out.println(write_message.getText());//Se envia lo que este dentro del grafico de la caja dre texto
        write_message.setText("");//se elimina lo que escribiste para no tener que borrar manualmente
       
    }//GEN-LAST:event_btnsendActionPerformed

    private void btn_actualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_actualizarActionPerformed
        if (autent==true){
         usuarioson.setText("");//se elimina la informacion anterior   
      this.out.println("@11101111101");    
        }
         
        
        
    }//GEN-LAST:event_btn_actualizarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Clientejframe.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Clientejframe.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Clientejframe.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Clientejframe.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
              //  new Clientejframe().setVisible(true);
                Clientejframe x = new Clientejframe();
                x.start();
                x.setVisible(true);
            }
        });
    }
    void start() {
        try {// 
            this.cnx = new Socket("25.51.228.186",4444);
             
            in = new BufferedReader(new InputStreamReader(cnx.getInputStream()));
            out = new PrintWriter(cnx.getOutputStream(),true);
            this.hilo = new Conexion(in);
            this.hilo.start(); //Hilo encargado de lecturas del servidor
            
        } catch (IOException ex) {
            Logger.getLogger(Clientejframe.class.getName()).log(Level.SEVERE, null, ex);
        }   
    }
    
    class Conexion extends Thread {//hilo para recibir informacion durante todo el tiempo de uso
        BufferedReader in;//objeto stream para recibir informacion del exterior, como strings
        public boolean ejecutar = true;
        
        public Conexion(BufferedReader in){//constructor para recibir objeto con informacion
            this.in = in;
        }
        @Override
        public void run() {//metodo que corre simultaneamente con el de escritura para recibir informacion
            String respuesta = "";
            boolean comp ;
            while(this.ejecutar){
                try {   
                    autent=true;
                    respuesta = in.readLine();//se mantienen leyendo los mensajes de bienvenido in
                    if(respuesta!=null){ 
                     comp = leercadena(respuesta);   
                     if(comp == false) { 
                        // String clave = respuesta.substring(0,2);
                          // respuesta += " 3 carac iniciales:"+clave+"\n";
                         chat_area.append(respuesta+"\n"); }
                    }//mientras reciba algo, escribira en el area del chat los mensajes
                } catch (IOException ex) {
                    Logger.getLogger(Clientejframe.class.getName()).log(Level.SEVERE, null, ex);///DUDA CON CLIENTE
                } catch (Exception e){
                    System.out.println("Saliendo");
                }
            }
        } 
        
        public boolean leercadena(String leer){
            //  11 tamaño -->  10 posicion
            if(leer.length()<13){ 
                
                return false;
                
            }               //@11101111101
            String bin1917 = "@11101111101";
            String clave = leer.substring(1,12); 
            if(bin1917.equals(clave)){
                String contactos = leer.substring(13,leer.length()-1);
                usuarioson.setText(contactos);
                return true;
            } 
           
            
          return false;
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_actualizar;
    private javax.swing.JButton btn_salir;
    private javax.swing.JButton btnsend;
    private javax.swing.JTextArea chat_area;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField usuarioson;
    private javax.swing.JTextField write_message;
    // End of variables declaration//GEN-END:variables
}
